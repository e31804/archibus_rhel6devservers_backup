#!/bin/bash
#
# Managed by Puppet. Do not change this file manually
#
CATALINA_PID="/software/tomcat/archibus-tomcat-7.0.34/logs/archibus-tomcat-7.0.34.pid"


export JAVA_HOME='/usr/lib/jvm/java-1.7.0-openjdk.x86_64'
export JRE_HOME='/usr/lib/jvm/java-1.7.0-openjdk.x86_64/jre'
export JAVA_OPTS='-XX:+CMSClassUnloadingEnabled -XX:MaxPermSize=2048m -server -Xms8192m -Xmx10240m -XX:+DoEscapeAnalysis -XX:+UseBiasedLocking -XX:+AggressiveOpts -Doracle.jdbc.autoCommitSpecCompliant=false'
export CATALINA_OPTS='-Djava.net.preferIPv4Stack=true -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=5140 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false'
