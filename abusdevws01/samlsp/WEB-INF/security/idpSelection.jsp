<%@ page import="org.springframework.security.saml.metadata.MetadataManager" %>
<%@ page import="org.springframework.web.context.WebApplicationContext" %>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%@ page import="java.util.Set" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<%
    WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletConfig().getServletContext());
    MetadataManager mm = context.getBean("metadata", MetadataManager.class);
    Set<String> idps = mm.getIDPEntityNames();
    pageContext.setAttribute("idp", idps);
%>

<p>
<form id="idp_form" action="<c:url value="${requestScope.idpDiscoReturnURL}"/>" method="GET">
    <table>
        <tr>
            <td><b></b></td>
            <td>
                <c:forEach var="idpItem" items="${idp}">
                    <input type="radio" name="${requestScope.idpDiscoReturnParam}" id="idp_<c:out value="${idpItem}"/>" value="<c:out value="${idpItem}"/>" style="display:none" />
                </c:forEach>
            </td>
        </tr>
    </table>
</form>
</p>
</body>
<%
out.println("<script language=\"javaScript\" type=\"text/javascript\">");
out.println("document.getElementsByName('idp')[0].checked=true;document.getElementById('idp_form').submit();");
out.println("</script>");  
%>
</html>